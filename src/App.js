import React from 'react';
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import MenuBar from "./Components/MenuBar";
import Footer from "./Components/Footer";
import Accueil from "./Components/Accueil";
import Simulateur from "./Components/Simulateur";
import PrivacyPolicy from './Components/Legal/PrivacyPolicy';
import TermsOfUse from './Components/Legal/TermsOfUse';

export default function App() {
    return (
        <Router>
            <MenuBar/>
            <Switch>
                <Route path="/simulateur">
                    <Simulateur/>
                </Route>
                <Route path="/privacy">
                    <PrivacyPolicy/>
                </Route>
                <Route path="/terms">
                    <TermsOfUse/>
                </Route>
                <Route path="/">
                    <Accueil/>
                </Route>
            </Switch>
            <Footer/>
        </Router>
    );
}
