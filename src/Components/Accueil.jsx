import {
    Button,
    Card,
    CardContent,
    CardHeader,
    CardMedia,
    Container,
    Grid,
    Icon,
    makeStyles,
    Typography
} from "@material-ui/core";
import {Link} from "react-router-dom";
import {Dns, LocalAtm} from "@material-ui/icons";
import Carousel from 'react-material-ui-carousel'
import {Parallax} from "react-parallax";
import React from "react";

const useStyles = makeStyles((theme) => ({
    '@global': {
        body: {
            overflowX: "hidden",
        },
    },
    parallax1: {
        paddingBottom: theme.spacing(6),
    },
    parallax2: {
        padding: theme.spacing(2, 0),
        marginBottom: theme.spacing(4),
    },
    header: {
        padding: theme.spacing(8, 0, 6),
    },
    contentCardGrid: {
        paddingTop: theme.spacing(8),
        paddingBottom: theme.spacing(8),
    },
    contentCard: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
    },
    contentCardIcon: {
        padding: theme.spacing(2, 0),
        fontSize: 60,
    },
    contentCardContent: {
        flexGrow: 1,
        color: theme.palette.text.primary,
    },
    carouselMedia: {
        height: "700px",
    },
    smallHeader: {
        padding: theme.spacing(6, 0),
    },
    rightContentCard: {
        marginLeft: "auto"
    },
    leftContentCard: {},
    contentCard2: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
        padding: theme.spacing(1),
    },
    pricingGrid: {
        marginTop: theme.spacing(4),
    },
    pricingCardHeader: {
        backgroundColor:
            theme.palette.type === 'light' ? theme.palette.grey[200] : theme.palette.grey[700],
    },
    pricingCardContent: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'baseline',
        marginBottom: theme.spacing(2),
    },
    buttons: {
        marginTop: theme.spacing(4),
    }
}));

const content1 = [
    {
        title: "Pratique",
        iconName: "tune",
        body: "Vous payez votre serveur à l'heure d'utilisation pour un maximum de flexibilité. La configuration de votre serveur s'effectue facilement au travers d'un panel d'administration complet.",
    },
    {
        title: "Économique",
        iconName: "local_atm",
        body: "Votre serveur peut être configuré pour s'éteindre automatiquement lorsque plus personne n'est connecté dessus afin de vous faire économiser de l'argent. Il peut aussi être configuré pour démarrer automatiquement lors de la connexion.",
    },
    {
        title: "Intelligent",
        iconName: "memory",
        body: "Vous pouvez suivre l'évolution du nombre de joueurs connectés et obtenir une prédiction sur les heures à venir afin d'ajuster la puissance du serveur.",
    },
]

const carousel = [
    {
        title: "Administration de vos serveur",
        body: "Toutes les informations importantes du serveur sont obtenables et paramétrables facilement",
        img: "img/list-servers.png"
    },
    {
      title: "Console de votre serveur",
      body: "Visualiser en temps réel les journaux de votre serveur et exécuter directement des commandes depuis le panel !",
      img: "img/console-server.png"
    },
    {
      title: "Analyser et surveiller",
      body: "Surveiller l'utilisation de vos ressources, le nombre de joueurs connecté et l'état de votre serveur",
      img: "img/graph-server.png"
    },
    {
      title: "Administration des joueurs",
      body: "Visualiser qui est connecté sur votre serveur, BAN, OP et UNBAN un joueur en 1 clic",
      img: "img/players-server.png"
    },
]

const content2 = [
    {
        title: "Performant",
        iconName: "speed",
        body: "Vos serveurs sont hébergés en Europe, optimisés et mis à jour par nos soins. Notre plateforme scalable s'adaptera toujours à votre besoin.",
    },
    {
        title: "Sur mesure",
        iconName: "tune",
        body: ["Vous attendez une augmentation du nombre de joueur un week-end ? Augmentez la RAM de votre serveur quand vous le souhaitez. Choisissez votre nom de domaine pour accéder au serveur."]
    },
    {
        title: "Complet",
        iconName: "check_circle_outline",
        body: "Créer, démarrer, arrêter et visualiser les logs de vos serveurs Minecraft n'a jamais été aussi facile avec notre Panel d'administration. Le nombre de joueurs par serveur n'est pas limité.",
    },
    {
        title: "Analyse & Monitoring",
        iconName: "multiline_chart",
        body: "Notre panel moderne vous permet de visualiser la consommation CPU et RAM de votre serveur. L'état, les journaux ainsi que les joueurs connectés sont aussi disponibles facilement.",
    }
]
const pricing = [
    {
        title: 'Basique',
        subheader: 'Pour un simple serveur entre amis',
        price: '0,01',
        description: [
            '1 Go de RAM',
            'Slots illimités',
            'Adapté pour un serveur Vanilla ou',
            'Spigot/Paper (quelques plugins)',
        ],
    },
    {
        title: 'Avancé',
        subheader: 'Pour rassembler une communauté',
        price: '0,04',
        description: [
            '4 Go de RAM',
            'Adapté pour un serveur Forge ou',
            'Spigot/Paper (plus de plugins)',
            'Base de données SQL',
        ],
    },
    {
        title: 'Pro',
        price: 'custom',
        subheader: 'Pour mettre en place un grand réseau de serveurs',
        description: [
            'Adapté pour du multi-serveurs',
            'avec BungeeCord',
            'Supporte une charge importante',
            'Infrastructure évolutive',
        ],
    },
];

export default function Accueil() {
    window.scrollTo(0, 0)
    const classes = useStyles();
    return (
        <main>
            <Container maxWidth="md" className={classes.header}>
                <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
                    Minecraft as a Service
                </Typography>
                <Typography variant="h5" align="center" color="textSecondary" paragraph>
                    Avec Minekloud, vous payez seulement pour le service que vous utilisez.
                </Typography>
                <Buttons/>
            </Container>

            <Parallax className={classes.parallax1} bgImage="img/bg1.png" blur={5} strength={500}>
                <Container className={classes.contentCardGrid} maxWidth="md">
                    <Grid container spacing={4}>
                        {content1.map((item, index) => <SmallCard value={item} index={index}/>)}
                    </Grid>
                </Container>
                <Container maxWidth="100%">
                    <Carousel autoPlay={true} timeout={350} animation={"slide"} navButtonsAlwaysVisible={true}>
                        {carousel.map((item, index) => <CarouselCard value={item} index={index}/>)}
                    </Carousel>
                </Container>
            </Parallax>

            <Container maxWidth="md" className={classes.smallHeader}>
                <Typography variant="h4" align="center" color="textPrimary" gutterBottom>
                    Performance & Flexibilité
                </Typography>
                <Typography variant="h5" align="center" color="textSecondary" paragraph>
                    Nous nous adaptons à tous vos besoins afin de vous satisfaire.
                </Typography>
            </Container>

            <Parallax className={classes.parallax2} bgImage="img/bg2.png" blur={5} strength={500}>
                <Container className={classes.contentCardGrid} maxWidth="lg">
                    <Grid container spacing={6}>
                        {content2.map((item, index) => <BigCard value={item} index={index}/>)}
                    </Grid>
                </Container>
            </Parallax>

            <Container maxWidth="md">
                <Typography variant="h4" align="center" color="textPrimary" gutterBottom>
                    Des prix avantageux
                </Typography>
                <Typography variant="h5" align="center" color="textSecondary" paragraph>
                    Notre offre s'applique sur mesure. Vous trouverez ci-dessous quelques exemples de prix, et vous
                    pouvez simuler le prix votre serveur à l'aide du simulateur.
                </Typography>
                <Grid container spacing={5} alignItems="flex-end" className={classes.pricingGrid}>
                    {pricing.map((item) => <PriceCard value={item}/>)}
                </Grid>
            </Container>
            <Buttons/>
        </main>
    )

    function Buttons() {
        return (
            <Grid container spacing={2} justify="center" className={classes.buttons}>
                <Grid item>
                    <a href="http://panel.minekloud.com">
                        <Button variant="contained" color="primary" size="large" startIcon={<Dns/>}>
                            Créer un serveur
                        </Button>
                    </a>
                </Grid>
                <Grid item>
                    <Link to="/simulateur">
                        <Button variant="outlined" color="primary" size="large" startIcon={<LocalAtm/>}>
                            Simulateur de prix
                        </Button>
                    </Link>
                </Grid>
            </Grid>
        )
    }

    function SmallCard(props) {
        return (
            <Grid item key={props.index} xs={12} sm={6} md={4}>
                <Card className={classes.contentCard}>
                    <CardContent className={classes.contentCardContent}>
                        <Typography align="center">
                            <Icon className={classes.contentCardIcon}>{props.value.iconName}</Icon>
                        </Typography>
                        <Typography gutterBottom variant="h5" component="h2" align="center">
                            {props.value.title}
                        </Typography>
                        <Typography align="center">
                            {props.value.body}
                        </Typography>
                    </CardContent>
                </Card>
            </Grid>
        )
    }

    function CarouselCard(props) {
        return (
            <Container maxWidth="lg">
                <Card key={props.index}>
                    <CardMedia className={classes.carouselMedia} image={props.value.img}/>
                    <CardContent>
                        <Typography gutterBottom variant="h5" component="h2">
                            {props.value.title}
                        </Typography>
                        <Typography variant="body2" color="textSecondary" component="p">
                            {props.value.body}
                        </Typography>
                    </CardContent>
                </Card>
            </Container>
        )
    }

    function BigCard(props) {
        const align = props.index % 2 ? "right" : "left";
        return (
            <Grid item key={props.index} md={10} sm={12} xs={12}
                  className={props.index % 2 ? classes.rightContentCard : classes.leftContentCard}>
                <Card className={classes.contentCard2}>
                    <CardContent className={classes.contentCardContent}>
                        <Typography align={align}>
                            <Icon className={classes.contentCardIcon}>{props.value.iconName}</Icon>
                        </Typography>
                        <Typography gutterBottom variant="h5" component="h2"
                                    align={align}>
                            {props.value.title}
                        </Typography>
                        <Typography align={align}>
                            {props.value.body}
                        </Typography>
                    </CardContent>
                </Card>
            </Grid>
        )
    }

    function PriceCard(props) {
        return (
            <Grid item key={props.value.title} xs={12} sm={6} md={4}>
                <Card>
                    <CardHeader
                        title={props.value.title}
                        subheader={props.value.subheader}
                        titleTypographyProps={{align: 'center'}}
                        subheaderTypographyProps={{align: 'center'}}
                        className={classes.pricingCardHeader}
                    />
                    <CardContent>
                        <div className={classes.pricingCardContent}>
                            <Typography component="h2" variant="h3" color="textPrimary">
                                €{props.value.price}
                            </Typography>
                            <Typography variant="h6" color="textSecondary">
                                /hr
                            </Typography>
                        </div>
                        <ul>
                            {props.value.description.map((line) => (
                                <Typography component="li" variant="subtitle1" align="center" key={line}>
                                    {line}
                                </Typography>
                            ))}
                        </ul>
                    </CardContent>
                </Card>
            </Grid>
        )
    }
}
