import React from "react";
import {Container, makeStyles, Slider, Typography, withStyles} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
    header: {
        padding: theme.spacing(8, 2, 6, 2),
    },
    containerContent: {
        padding: theme.spacing(4, 2),
    },
}));

const CustomSlider = withStyles({
    mark: {
        backgroundColor: '#3f51b5',
        height: 10,
        width: 3,
        marginTop: -4,
    }
})(Slider);

const ramSliderMarks = [
    {
        value: 1,
        label: "1 GB",
    },
    {
        value: 2,
        label: "2 GB",
    },
    {
        value: 4,
        label: "4 GB",
    },
    {
        value: 8,
        label: "8 GB",
    }
]

const hourSliderMarks = [
    {
        value: 1,
        label: "1h",
    },
    {
        value: 4,
        label: "4h",
    },
    {
        value: 12,
        label: "12h",
    },
    {
        value: 24,
        label: "24h",
    },
]

const euroPerRamPerHour = 0.01;

export default function Simulateur() {
    window.scrollTo(0, 0)
    const [ram, setRam] = React.useState(2);
    const [hours, setHours] = React.useState(12);
    const changeRam = (event, value) => {
        setRam(value);
    }
    const changeHours = (event, value) => {
        setHours(value);
    }
    const classes = useStyles();
    let pricePerHour = ram * euroPerRamPerHour;
    let pricePerHourString = pricePerHour.toFixed(2);
    let pricePerDay = pricePerHour * hours;
    let pricePerDayString = pricePerDay.toFixed(2);
    let pricePerMonth = pricePerDay * 30;
    let pricePerMonthString = pricePerMonth.toFixed(1);
    return (
        <main>
            <Container maxWidth="md" className={classes.header}>
                <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
                    Simulateur de prix
                </Typography>
                <Typography variant="h5" align="center" color="textSecondary" paragraph>
                    Testez notre simulateur afin d'estimer le prix de votre serveur.
                </Typography>
            </Container>
            <Container maxWidth="md" className={classes.containerContent}>
                <Typography variant="h4" gutterBottom>
                    RAM allouée au serveur : {ram} GB
                </Typography>
                <CustomSlider
                    defaultValue={ram}
                    step={0.5}
                    marks={ramSliderMarks}
                    min={0.5}
                    max={8}
                    valueLabelDisplay="auto"
                    onChange={changeRam}
                    style={{
                        mark: {
                            backgroundColor: '#bfbfbf',
                            height: 8,
                            width: 1,
                            marginTop: -3,
                        },
                    }}
                />
                <Typography variant="h5">
                    {pricePerHourString}€/heure
                </Typography>
            </Container>
            <Container maxWidth="md" className={classes.containerContent}>
                <Typography variant="h4" gutterBottom>
                    Temps moyen d'utilisation du serveur : {hours}h par jour
                </Typography>
                <CustomSlider
                    defaultValue={hours}
                    min={1}
                    max={24}
                    marks={hourSliderMarks}
                    valueLabelDisplay="auto"
                    onChange={changeHours}
                />
                <Typography variant="h5">
                    {pricePerDayString}€/jour<br/>
                    soit environ {pricePerMonthString}€/mois
                </Typography>
            </Container>
        </main>
    );
}